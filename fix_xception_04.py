import torch
import pretrainedmodels as ptm

state_dict = torch.load('./xception-b5690688.pth')

for name, weights in state_dict.items():
    if 'pointwise' in name:
        state_dict[name] = weights.unsqueeze(-1).unsqueeze(-1)
        print ('fixed', weights.size())

torch.save(state_dict, 'xception-fixed.pth')