import torch
from torch.utils.data import DataLoader
from torchvision import transforms
import os
import utils
from augmentation import five_crops, HorizontalFlip, make_transforms, Rotate
from misc import ZaloDataset, preprocess, NB_CLASSES, preprocess_hflip, normalize_05, normalize_torch, preprocess_rotate
from cnn_finetune import make_model
import torch.backends.cudnn as cudnn
#os.environ['CUDA_VISIBLE_DEVICES'] = '0'  #'1'

BATCH_SIZE = 64
TTA = False
use_gpu = torch.cuda.is_available()
device = torch.device('cuda' if use_gpu else 'cpu')

def get_model(model_class):
    print('[+] loading model... ', end='', flush=True)
    model = model_class(NB_CLASSES)
    if use_gpu:
        model.cuda()
    print('done')
    return model

def gen_outputline(fn, preds):
    idx = fn.split('/')[-1][:-4]
    return  idx + ','+ str(preds)[1:-1].replace(',', '') +  '\n'

def predict_cnn_finetune(model_name, weight_pth, save_dir, image_size, normalize):
    print(f'[+] predict {model_name}')
    model = make_model(
        model_name,
        pretrained=False,
        num_classes=NB_CLASSES,
        input_size=(image_size, image_size)
    )
    model = model.to(device)
    checkpoint_dict = torch.load(weight_pth)
    best_epoch = checkpoint_dict['epoch']
    model.load_state_dict(checkpoint_dict['state_dict'])
    model.eval()
    print('Stop at epoch ', best_epoch)
    model.eval()

    data_dir = './zalo_landmark/Public/'
    fn_all = [data_dir + fn for fn in os.listdir(data_dir) if fn.endswith('.jpg')]
    fns = []
    fn_corrupted = []
    for fn in fn_all:
        # filter dammaged images
        if os.path.getsize(fn) > 0:
            fns.append(fn)
        else:
            fn_corrupted.append(fn)

    print('Total provided files: {}'.format(len(fn_all)))
    print('Total damaged files: {}'.format(len(fn_all) - len(fns)))
    print("Damaged files: {}".format(fn_corrupted))

    lbs = [-1] * len(fns)

    tta_preprocess = [
        preprocess(normalize, image_size),
        preprocess_hflip(normalize, image_size),
        # preprocess_rotate(normalize, image_size)
    ]
    tta_preprocess += make_transforms(
        [transforms.Resize((image_size + 20, image_size + 20))],
        [transforms.ToTensor(), normalize],
        five_crops(image_size))
    tta_preprocess += make_transforms(
        [transforms.Resize((image_size + 20, image_size + 20))],
        [HorizontalFlip(), transforms.ToTensor(), normalize],
        five_crops(image_size))
    # tta_preprocess += make_transforms(
    #     [transforms.Resize((image_size + 20, image_size + 20))],
    #     [Rotate(), transforms.ToTensor(), normalize],
    #      five_crops(image_size))
    print(f'[+] tta size: {len(tta_preprocess)}')

    data_loaders = []
    for transform in tta_preprocess:
        # test_dataset = FurnitureDataset('test', transform=transform)
        test_dataset = ZaloDataset(fns, lbs, transform=transform)
        data_loader = DataLoader(dataset=test_dataset, num_workers=8,
                                 batch_size=BATCH_SIZE,
                                 shuffle=False)
        data_loaders.append(data_loader)

    lx, px = utils.predict_tta(model, data_loaders)
    data = {
        'lx': lx.cpu(),
        'px': px.cpu(),
        'fns': fns,
        'fn_corrupted': fn_corrupted
    }
    torch.save(data,os.path.join(save_dir, f'{model_name}_test_prediction_full.pth'))

def predict_all_cnn_finetune(model_name, train_dir, save_dir, image_size):
    predict_cnn_finetune(model_name, os.path.join(train_dir, model_name+'_best_full.pth.tar'), save_dir, image_size, normalize_05)

if __name__ == "__main__":
    network_list = ['se_resnext50_32x4d', 'inception_v4', 'xception', 'densenet161']
    image_size_list = [320, 320, 320, 288]
    train_dir ='./results'
    save_dir ='./results_prediction'
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)
    for ind, network in enumerate(network_list):
        predict_all_cnn_finetune(network, train_dir, save_dir, image_size_list[ind])
